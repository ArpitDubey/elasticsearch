package com.ttn.ms.search.repository;

import com.ttn.ms.search.entity.IndexData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Set;

public interface IndexDataRepository extends ElasticsearchRepository<IndexData, String> {
    Page<IndexData> findDistinctByColorCodesIn(Set<Integer> colorCodes, Pageable pageable);
    List<IndexData> findDistinctByColorCodesIn(Set<Integer> colorCodes);
    List<IndexData> findDistinctByAccountIdInAndColorCodesIn(List<String> accountId,Set<Integer> colorCodes);
}