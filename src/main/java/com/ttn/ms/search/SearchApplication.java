package com.ttn.ms.search;

import com.ttn.ms.search.entity.IndexData;
import com.ttn.ms.search.service.IDataIndexService;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@SpringBootApplication
@ComponentScan(basePackages = {"com.ttn.ms.search"})
@EnableElasticsearchRepositories(basePackages = "com.ttn.ms.search.repository")
public class SearchApplication implements CommandLineRunner {

    @Autowired
    private ElasticsearchOperations es;

    @Autowired
    private IDataIndexService dataIndexService;

    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        printElasticSearchInfo();
    }

    private void printElasticSearchInfo() {
        System.out.println("--ElasticSearch---------------------------------------------");
        Client client = es.getClient();
        Map<String, String> asMap = client.settings().getAsMap();
        asMap.forEach((k, v) -> {
            System.out.println(k + " = " + v);
        });
        System.out.println("--ElasticSearch---------------------------------------------");
    }

}
