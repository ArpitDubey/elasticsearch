package com.ttn.ms.search.entity;

import com.ttn.ms.search.co.IndexDataCO;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;
import java.util.Set;

@Document(indexName = "garble-cloud", type = "indexed-data")
public class IndexData {

    @Id
    private String id;
    private String fileId;
    private String accountId;
    private String cloudStorageType;
    private String link;
    private String mimeType;
    private String name;
    private String ownersName;
    private String ownersEmail;
    private boolean isGDOC;
    private boolean secureShare;
    private boolean shared;
    private Long modified;
    private Set<Integer> colorCodes;
    private String bloomfilter1;
    private String bloomfilter2;

    public String getBloomfilter1() {
        return bloomfilter1;
    }

    public void setBloomfilter1(String bloomfilter1) {
        this.bloomfilter1 = bloomfilter1;
    }

    public String getBloomfilter2() {
        return bloomfilter2;
    }

    public void setBloomfilter2(String bloomfilter2) {
        this.bloomfilter2 = bloomfilter2;
    }

    public void setCloudStorageType(String cloudStorageType) {
        this.cloudStorageType = cloudStorageType;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCloudStorageType() {
        return cloudStorageType;
    }

    public String getLink() {
        return link;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getName() {
        return name;
    }

    public String getOwnersName() {
        return ownersName;
    }

    public String getOwnersEmail() {
        return ownersEmail;
    }

    public boolean isGDOC() {
        return isGDOC;
    }

    public boolean isSecureShare() {
        return secureShare;
    }

    public boolean isShared() {
        return shared;
    }

    public Long getModified() {
        return modified;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwnersName(String ownersName) {
        this.ownersName = ownersName;
    }

    public void setOwnersEmail(String ownersEmail) {
        this.ownersEmail = ownersEmail;
    }

    public void setGDOC(boolean GDOC) {
        isGDOC = GDOC;
    }

    public void setSecureShare(boolean secureShare) {
        this.secureShare = secureShare;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getFileId() {
        return fileId;
    }

    public Set<Integer> getColorCodes() {
        return colorCodes;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setColorCodes(Set<Integer> colorCodes) {
        this.colorCodes = colorCodes;
    }

    public IndexData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public IndexData(IndexDataCO co) {
        this.accountId = co.getAccountId();
        this.colorCodes = co.getColorCodes();
        this.cloudStorageType=co.getCloudStorageType();
        this.fileId = co.getFileId();
        this.isGDOC=co.isGDOC();
        this.link=co.getLink();
        this.mimeType=co.getMimeType();
        this.modified=System.currentTimeMillis();
        this.name=co.getName();
        this.ownersEmail=co.getOwnersEmail();
        this.ownersName=co.getOwnersName();
        this.secureShare=co.isSecureShare();
        this.shared=co.isShared();
        this.bloomfilter1=co.getBloomfilter1();
        this.bloomfilter2=co.getBloomfilter2();
    }


    public IndexData(IndexData dump,String accountId) {
        this.accountId = accountId;
        this.colorCodes = dump.getColorCodes();
        this.cloudStorageType=dump.getCloudStorageType();
        this.fileId = dump.getFileId();
        this.isGDOC=dump.isGDOC();
        this.link=dump.getLink();
        this.mimeType=dump.getMimeType();
        this.modified=System.currentTimeMillis();
        this.name=dump.getName();
        this.ownersEmail=dump.getOwnersEmail();
        this.ownersName=dump.getOwnersName();
        this.secureShare=dump.isSecureShare();
        this.shared=dump.isShared();
        this.bloomfilter1=dump.getBloomfilter1();
        this.bloomfilter2=dump.getBloomfilter2();
    }

    @Override
    public String toString() {
        return "IndexData{" +
                "id='" + id + '\'' +
                ", accountId='" + accountId + '\'' +
                ", cloudStorageType='" + cloudStorageType + '\'' +
                ", link='" + link + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", name='" + name + '\'' +
                ", ownersName='" + ownersName + '\'' +
                ", ownersEmail='" + ownersEmail + '\'' +
                ", isGDOC=" + isGDOC +
                ", secureShare=" + secureShare +
                ", shared=" + shared +
                ", modified=" + modified +
                ", fileId='" + fileId + '\'' +
                ", colorCodes=" + colorCodes +
                '}';
    }
}
