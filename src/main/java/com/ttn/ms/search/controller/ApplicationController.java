package com.ttn.ms.search.controller;

import com.ttn.ms.search.co.IndexDataCO;
import com.ttn.ms.search.entity.IndexData;
import com.ttn.ms.search.service.IDataIndexService;
import com.ttn.ms.search.utilis.ResultSet;
import com.ttn.ms.search.vo.QueryParseVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
public class ApplicationController {
    private final Logger log = LoggerFactory.getLogger(ApplicationController.class);

    @Autowired
    private IDataIndexService dataIndexService;

    @RequestMapping(value = "/indexdata")
    @ResponseBody
    public IndexData indexing(@RequestBody IndexDataCO indexDataCO) {
        return dataIndexService.save(indexDataCO);
    }

    @RequestMapping(value = "/search")
    @ResponseBody
    public ResultSet search(@RequestBody QueryParseVO vo) {
        System.out.println(vo);
        return dataIndexService.search(vo);
    }

    @RequestMapping(value = "/dump")
    public String dump() {
        dataIndexService.dump();
        return "dumped";
    }

    @RequestMapping(value = "/copy")
    public String copy() {
        dataIndexService.copy();
        return "copied";
    }

}
