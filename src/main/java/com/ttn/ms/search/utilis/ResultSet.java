package com.ttn.ms.search.utilis;

import java.util.LinkedHashMap;
import java.util.Map;

public class ResultSet {

	private Integer code;
	private String error;
	private Map<String, Object> data = new LinkedHashMap<String, Object>();
	
	
	public ResultSet() {}
	
	public ResultSet(Integer code) {
		this.code = code;
	}
	
	public ResultSet(Integer code, String error) {
		this.code = code;
		this.error = error;
	}

	public ResultSet(Integer code, String key, Object value) {
		this.code = code;
		if (data != null) {
			data.put(key, value);
		}
	}

	public Map<String, Object> getData() {
		return data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void put(String key, Object value) {
		if (data != null) {
			data.put(key, value);
		}
	}
}
