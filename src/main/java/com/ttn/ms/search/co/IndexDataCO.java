package com.ttn.ms.search.co;

import java.util.List;
import java.util.Set;

public class IndexDataCO {

    private String accountId;
    private String cloudStorageType;
    private String link;
    private String mimeType;
    private String name;
    private String ownersName;
    private String ownersEmail;
    private boolean isGDOC;
    private boolean secureShare;
    private boolean shared;
    private Long modified;
    private String fileId;
    private Set<Integer> colorCodes;
    private String bloomfilter1;
    private String bloomfilter2;

    public String getBloomfilter1() {
        return bloomfilter1;
    }

    public void setBloomfilter1(String bloomfilter1) {
        this.bloomfilter1 = bloomfilter1;
    }

    public String getBloomfilter2() {
        return bloomfilter2;
    }

    public void setBloomfilter2(String bloomfilter2) {
        this.bloomfilter2 = bloomfilter2;
    }

    public void setCloudStorageType(String cloudStorageType) {
        this.cloudStorageType = cloudStorageType;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCloudStorageType() {
        return cloudStorageType;
    }

    public String getLink() {
        return link;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getName() {
        return name;
    }

    public String getOwnersName() {
        return ownersName;
    }

    public String getOwnersEmail() {
        return ownersEmail;
    }

    public boolean isGDOC() {
        return isGDOC;
    }

    public boolean isSecureShare() {
        return secureShare;
    }

    public boolean isShared() {
        return shared;
    }

    public Long getModified() {
        return modified;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwnersName(String ownersName) {
        this.ownersName = ownersName;
    }

    public void setOwnersEmail(String ownersEmail) {
        this.ownersEmail = ownersEmail;
    }

    public void setGDOC(boolean GDOC) {
        isGDOC = GDOC;
    }

    public void setSecureShare(boolean secureShare) {
        this.secureShare = secureShare;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getFileId() {
        return fileId;
    }

    public Set<Integer> getColorCodes() {
        return colorCodes;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setColorCodes(Set<Integer> colorCodes) {
        this.colorCodes = colorCodes;
    }
}
