package com.ttn.ms.search.service;

import com.ttn.ms.search.co.IndexDataCO;
import com.ttn.ms.search.entity.IndexData;
import com.ttn.ms.search.utilis.ResultSet;
import com.ttn.ms.search.vo.QueryParseVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface IDataIndexService {

    IndexData save(IndexDataCO indexDataCO);

    IndexData save(IndexData indexData);

    void delete(IndexData indexData);

    IndexData findOne(String id);

    Iterable<IndexData> findAll();

    Page<IndexData> search(QueryParseVO vo, PageRequest pageRequest);

    ResultSet search(QueryParseVO vo);

    void dump();

    void copy();

}
