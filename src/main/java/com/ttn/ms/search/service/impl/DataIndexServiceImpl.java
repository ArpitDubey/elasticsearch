package com.ttn.ms.search.service.impl;

import com.ttn.ms.search.client.Base64Coder;
import com.ttn.ms.search.client.EvaluateArithmeticExpression;
import com.ttn.ms.search.co.IndexDataCO;
import com.ttn.ms.search.entity.IndexData;
import com.ttn.ms.search.repository.IndexDataRepository;
import com.ttn.ms.search.service.IDataIndexService;
import com.ttn.ms.search.utilis.ResultSet;
import com.ttn.ms.search.vo.QueryParseVO;
import com.ttn.ms.search.vo.SearchResultVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import secgoogdocs.client.BloomFilter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataIndexServiceImpl implements IDataIndexService {

    @Autowired
    IndexDataRepository indexDataRepository;

    public void setIndexDataRepository(IndexDataRepository indexDataRepository) {
        this.indexDataRepository = indexDataRepository;
    }

    @Override
    public IndexData save(IndexDataCO co) {
        IndexData indexData = new IndexData(co);
        return save(indexData);
    }

    @Override
    public void delete(IndexData indexData) {

    }

    @Override
    public IndexData findOne(String id) {
        return null;
    }

    @Override
    public Iterable<IndexData> findAll() {
        return indexDataRepository.findAll();
    }

    private Iterable<IndexData> findAll(int page,int size) {
        return indexDataRepository.findAll(PageRequest.of(page,size));
    }


    public long count() {
        return indexDataRepository.count();
    }

    @Override
    public IndexData save(IndexData indexData) {
        return indexDataRepository.save(indexData);
    }

    @Override
    public Page<IndexData> search(QueryParseVO vo, PageRequest pageRequest) {
        return indexDataRepository.findDistinctByColorCodesIn(vo.getColorcodes(), pageRequest);
    }

    @Override
    public ResultSet search(QueryParseVO vo) {
        String SUCCESS_MESSAGE_KEY = "message";
        Integer SUCCESS_CODE = 1;
        String DRIVE_FILES_KEY = "files";
        ResultSet result = new ResultSet(SUCCESS_CODE);

        System.out.println("vo.getAccountIds() " + vo.getAccountIds());
        System.out.println("vo.getColorcodes() " + vo.getColorcodes());

        List<IndexData> indexDatas = indexDataRepository.findDistinctByAccountIdInAndColorCodesIn(vo.getAccountIds(), vo.getColorcodes());
        List<IndexData> files = null;
        if (indexDatas != null) {
            System.out.println("Results from DB size " + indexDatas.size());
            files = indexDatas.stream().filter(indexData -> {
                System.out.println("indexData file id " + indexData.getFileId());
                return bloomFilterAndCheckValidity(vo, indexData);
            }).collect(Collectors.toList());
        }

        if (files != null) {
            System.out.println("Bloom filtered data size " + files.size());
            List<SearchResultVO> searchResults = new ArrayList<>();
            int index = 0;
            for (IndexData indexData : files) {
                searchResults.add(new SearchResultVO(indexData, index++));
            }
            System.out.println("searchResults size " + searchResults.size());
            result.put(DRIVE_FILES_KEY, searchResults);
        }
        return result;
    }


    public static boolean bloomFilterAndCheckValidity(QueryParseVO vo, IndexData indexData) {
        String WORD_PAD = "sSsSzz";
        String boolean_arithmetic_expression = new String(vo.getBooleanQuery());
        try {
            BloomFilter<String> deserializedBloom1 = deserializeFromString(indexData.getBloomfilter1());
            BloomFilter<String> deserializedBloom2 = deserializeFromString(indexData.getBloomfilter2());
            for (String keyword : vo.getTermSet()) {
                String replaceString = WORD_PAD + keyword + WORD_PAD;
                System.out.println("deserializedBloom1 " + keyword + " contains " + deserializedBloom1.contains(keyword));
                System.out.println("deserializedBloom2 " + keyword + " contains " + deserializedBloom2.contains(keyword));
                if ((deserializedBloom1.contains(keyword) && deserializedBloom2.contains(keyword))
                        || StringUtils.containsIgnoreCase("File Name", keyword)) {
                    boolean_arithmetic_expression = boolean_arithmetic_expression.replaceAll(replaceString, " 1 ");
                } else {
                    boolean_arithmetic_expression = boolean_arithmetic_expression.replaceAll(replaceString, "0");
                }
            }

            double expression_value = EvaluateArithmeticExpression.evaluate(boolean_arithmetic_expression);


            double b_value = expression_value;
            if (b_value != 0.0) {
                System.out.println("Added! ");
                return true;
            }
            System.out.println("expression_value " + expression_value);
            System.out.println("boolean_arithmetic_expression " + boolean_arithmetic_expression);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e);
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public void dump(){
        int size=200;
        long counts=count();
        int expression=(int)counts/size;
        for(int i=0;i<=expression;i++) {
            System.out.println("count is "+count());
            Iterable<IndexData> indexDatas = findAll(i, size);
            for (IndexData indexdata : indexDatas) {
                persit(indexdata);
            }
        }
    }

    void persit(IndexData indexdata){
        List<String> accountIds = Arrays.asList("110640964322029124118","106443154698085180384","110113316033099359097","107148269709697747710","107977212073085295128");
        for(String accountId:accountIds) {
            IndexData indexDump1 = new IndexData(indexdata, accountId);
            save(indexDump1);
            IndexData indexDump2 = new IndexData(indexdata, "109908631032760776276");
            save(indexDump2);
        }
    }



    @Override
    public void copy(){
        int size=200;
        long counts=count();
        int expression=(int)counts/size;
        for(int i=0;i<=expression;i++) {
            System.out.println("count is "+count());
            Iterable<IndexData> indexDatas = findAll(i, size);
            for (IndexData indexdata : indexDatas) {
                IndexData indexDump = new IndexData(indexdata, indexdata.getAccountId());
                save(indexDump);
            }
        }
    }



    @SuppressWarnings("unchecked")
    private static BloomFilter<String> deserializeFromString(String s) throws IOException, ClassNotFoundException {
        byte[] data = Base64Coder.decode(s);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        return (BloomFilter<String>) o;
    }

}
