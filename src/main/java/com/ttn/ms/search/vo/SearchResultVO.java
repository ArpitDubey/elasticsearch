package com.ttn.ms.search.vo;

import com.ttn.ms.search.entity.IndexData;

public class SearchResultVO {

    private String accountId;
    private String cloudStorageType;
    private String link;
    private String mimeType;
    private String userId;
    private String name;
    private String ownersName;
    private String ownersEmail;
    private boolean isGDOC;
    private boolean locked;
    private boolean secureShare;
    private boolean shared;
    private Long modified;
    private String id;
    private int index;

    public SearchResultVO(IndexData indexData,int index) {
        this.cloudStorageType = indexData.getCloudStorageType();
        this.link = indexData.getLink();
        this.mimeType = indexData.getMimeType();
        this.name = indexData.getName();
        this.modified = indexData.getModified();

        this.isGDOC = indexData.isGDOC();
        this.secureShare = indexData.isSecureShare();
        this.shared = indexData.isShared();

        this.index = index;
        this.id = indexData.getFileId();
        this.accountId = indexData.getAccountId();
        this.userId = indexData.getAccountId();
        this.locked = isLocked(indexData.getName());

        this.ownersName = indexData.getOwnersName();
        this.ownersEmail = indexData.getOwnersEmail();
    }

    public String getAccountId() {
        return accountId;
    }

    public String getCloudStorageType() {
        return cloudStorageType;
    }

    public String getLink() {
        return link;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getOwnersName() {
        return ownersName;
    }

    public String getOwnersEmail() {
        return ownersEmail;
    }

    public boolean isGDOC() {
        return isGDOC;
    }

    public boolean isLocked() {
        return locked;
    }

    public boolean isSecureShare() {
        return secureShare;
    }

    public boolean isShared() {
        return shared;
    }

    public Long getModified() {
        return modified;
    }

    public String getId() {
        return id;
    }

    public int getIndex() {
        return index;
    }

    private boolean isLocked(String name){
        if (name != null) {
            return name.endsWith(".enc");
        }
        return false;
    }

}
