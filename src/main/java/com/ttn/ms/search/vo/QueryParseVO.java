package com.ttn.ms.search.vo;

import java.util.List;
import java.util.Set;

public class QueryParseVO {
	private List<String> accountIds;
	private String booleanQuery;
	private Set<String> termSet;
	private Set<Integer> colorcodes;


	public List<String> getAccountIds() {
		return accountIds;
	}
	public void setAccountIds(List<String> accountIds) {
		this.accountIds = accountIds;
	}
	public Set<Integer> getColorcodes() {
		return colorcodes;
	}
	public void setColorcodes(Set<Integer> colorcodes) {
		this.colorcodes = colorcodes;
	}
	public String getBooleanQuery() {
		return booleanQuery;
	}
	public void setBooleanQuery(String booleanQuery) {
		this.booleanQuery = booleanQuery;
	}
	public Set<String> getTermSet() {
		return termSet;
	}
	public void setTermSet(Set<String> termSet) {
		this.termSet = termSet;
	}

	@Override
	public String toString() {
		return "QueryParseVO{" +
				", booleanQuery='" + booleanQuery + '\'' +
				", termSet=" + termSet +
				", colorcodes=" + colorcodes +
				", accountIds=" + accountIds +
				'}';
	}

}
